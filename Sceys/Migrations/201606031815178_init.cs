namespace Sceys.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aprendizs",
                c => new
                    {
                        AprendizId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.AprendizId);
            
            CreateTable(
                "dbo.Expedientes",
                c => new
                    {
                        ExpedienteId = c.Int(nullable: false, identity: true),
                        InformanteId = c.Int(nullable: false),
                        AprendizId = c.Int(nullable: false),
                        RecepcionDocumentoId = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ExpedienteId)
                .ForeignKey("dbo.Aprendizs", t => t.AprendizId, cascadeDelete: true)
                .ForeignKey("dbo.Informantes", t => t.InformanteId, cascadeDelete: true)
                .ForeignKey("dbo.RecepcionDocumentoes", t => t.RecepcionDocumentoId, cascadeDelete: true)
                .Index(t => t.InformanteId)
                .Index(t => t.AprendizId)
                .Index(t => t.RecepcionDocumentoId);
            
            CreateTable(
                "dbo.Informantes",
                c => new
                    {
                        InformanteId = c.Int(nullable: false, identity: true),
                        PersonaId = c.Int(nullable: false),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.InformanteId)
                .ForeignKey("dbo.Personas", t => t.PersonaId, cascadeDelete: true)
                .Index(t => t.PersonaId);
            
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        PersonaId = c.Int(nullable: false, identity: true),
                        TipoDocumentoId = c.Int(nullable: false),
                        Documento = c.Int(nullable: false),
                        PrimerNombre = c.String(nullable: false, maxLength: 20),
                        SegundoNombre = c.String(maxLength: 20),
                        PrimerApellido = c.String(nullable: false, maxLength: 20),
                        SegundoApellido = c.String(maxLength: 20),
                        Correo = c.String(nullable: false, maxLength: 50),
                        Direccion = c.String(nullable: false),
                        Celular = c.String(maxLength: 15),
                        Telefono = c.String(maxLength: 15),
                        Votante = c.Boolean(nullable: false),
                        Estado = c.Boolean(nullable: false),
                        TipoPersonaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PersonaId)
                .ForeignKey("dbo.TipoDocumentoes", t => t.TipoDocumentoId, cascadeDelete: true)
                .ForeignKey("dbo.TipoPersonas", t => t.TipoPersonaId, cascadeDelete: true)
                .Index(t => t.TipoDocumentoId)
                .Index(t => t.TipoPersonaId);
            
            CreateTable(
                "dbo.TipoDocumentoes",
                c => new
                    {
                        TipoDocumentoId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TipoDocumentoId);
            
            CreateTable(
                "dbo.TipoPersonas",
                c => new
                    {
                        TipoPersonaId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TipoPersonaId);
            
            CreateTable(
                "dbo.RecepcionDocumentoes",
                c => new
                    {
                        RecepcionDocumentoId = c.Int(nullable: false, identity: true),
                        AreaId = c.Int(nullable: false),
                        PersonaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RecepcionDocumentoId)
                .ForeignKey("dbo.Areas", t => t.AreaId, cascadeDelete: true)
                .ForeignKey("dbo.Personas", t => t.PersonaId, cascadeDelete: true)
                .Index(t => t.AreaId)
                .Index(t => t.PersonaId);
            
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        AreaId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.AreaId);
            
            CreateTable(
                "dbo.Centroes",
                c => new
                    {
                        CentroId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 250),
                        RegionalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CentroId)
                .ForeignKey("dbo.Regionals", t => t.RegionalId, cascadeDelete: true)
                .Index(t => t.RegionalId);
            
            CreateTable(
                "dbo.ProgramaCentroes",
                c => new
                    {
                        ProgramaCentroId = c.Int(nullable: false, identity: true),
                        ProgramaId = c.Int(nullable: false),
                        CentroId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProgramaCentroId)
                .ForeignKey("dbo.Centroes", t => t.CentroId, cascadeDelete: true)
                .ForeignKey("dbo.Programas", t => t.ProgramaId, cascadeDelete: true)
                .Index(t => t.ProgramaId)
                .Index(t => t.CentroId);
            
            CreateTable(
                "dbo.Programas",
                c => new
                    {
                        ProgramaId = c.Int(nullable: false, identity: true),
                        TipoProgramaId = c.Int(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.ProgramaId)
                .ForeignKey("dbo.TipoProgramas", t => t.TipoProgramaId, cascadeDelete: true)
                .Index(t => t.TipoProgramaId);
            
            CreateTable(
                "dbo.TipoProgramas",
                c => new
                    {
                        TipoProgramaId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.TipoProgramaId);
            
            CreateTable(
                "dbo.Regionals",
                c => new
                    {
                        RegionalId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.RegionalId);
            
            CreateTable(
                "dbo.Documentacions",
                c => new
                    {
                        DocumentacionId = c.Int(nullable: false, identity: true),
                        TipoDocumentacionId = c.Int(nullable: false),
                        Aportante = c.String(),
                        Documento = c.String(),
                    })
                .PrimaryKey(t => t.DocumentacionId)
                .ForeignKey("dbo.TipoDocumentacions", t => t.TipoDocumentacionId, cascadeDelete: true)
                .Index(t => t.TipoDocumentacionId);
            
            CreateTable(
                "dbo.Informes",
                c => new
                    {
                        InformeId = c.Int(nullable: false, identity: true),
                        ExpedienteId = c.Int(nullable: false),
                        DocumentacionId = c.Int(nullable: false),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.InformeId)
                .ForeignKey("dbo.Documentacions", t => t.DocumentacionId, cascadeDelete: true)
                .Index(t => t.DocumentacionId);
            
            CreateTable(
                "dbo.TipoDocumentacions",
                c => new
                    {
                        TipoDocumentacionId = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.TipoDocumentacionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documentacions", "TipoDocumentacionId", "dbo.TipoDocumentacions");
            DropForeignKey("dbo.Informes", "DocumentacionId", "dbo.Documentacions");
            DropForeignKey("dbo.Centroes", "RegionalId", "dbo.Regionals");
            DropForeignKey("dbo.Programas", "TipoProgramaId", "dbo.TipoProgramas");
            DropForeignKey("dbo.ProgramaCentroes", "ProgramaId", "dbo.Programas");
            DropForeignKey("dbo.ProgramaCentroes", "CentroId", "dbo.Centroes");
            DropForeignKey("dbo.RecepcionDocumentoes", "PersonaId", "dbo.Personas");
            DropForeignKey("dbo.Expedientes", "RecepcionDocumentoId", "dbo.RecepcionDocumentoes");
            DropForeignKey("dbo.RecepcionDocumentoes", "AreaId", "dbo.Areas");
            DropForeignKey("dbo.Personas", "TipoPersonaId", "dbo.TipoPersonas");
            DropForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TipoDocumentoes");
            DropForeignKey("dbo.Informantes", "PersonaId", "dbo.Personas");
            DropForeignKey("dbo.Expedientes", "InformanteId", "dbo.Informantes");
            DropForeignKey("dbo.Expedientes", "AprendizId", "dbo.Aprendizs");
            DropIndex("dbo.Informes", new[] { "DocumentacionId" });
            DropIndex("dbo.Documentacions", new[] { "TipoDocumentacionId" });
            DropIndex("dbo.Programas", new[] { "TipoProgramaId" });
            DropIndex("dbo.ProgramaCentroes", new[] { "CentroId" });
            DropIndex("dbo.ProgramaCentroes", new[] { "ProgramaId" });
            DropIndex("dbo.Centroes", new[] { "RegionalId" });
            DropIndex("dbo.RecepcionDocumentoes", new[] { "PersonaId" });
            DropIndex("dbo.RecepcionDocumentoes", new[] { "AreaId" });
            DropIndex("dbo.Personas", new[] { "TipoPersonaId" });
            DropIndex("dbo.Personas", new[] { "TipoDocumentoId" });
            DropIndex("dbo.Informantes", new[] { "PersonaId" });
            DropIndex("dbo.Expedientes", new[] { "RecepcionDocumentoId" });
            DropIndex("dbo.Expedientes", new[] { "AprendizId" });
            DropIndex("dbo.Expedientes", new[] { "InformanteId" });
            DropTable("dbo.TipoDocumentacions");
            DropTable("dbo.Informes");
            DropTable("dbo.Documentacions");
            DropTable("dbo.Regionals");
            DropTable("dbo.TipoProgramas");
            DropTable("dbo.Programas");
            DropTable("dbo.ProgramaCentroes");
            DropTable("dbo.Centroes");
            DropTable("dbo.Areas");
            DropTable("dbo.RecepcionDocumentoes");
            DropTable("dbo.TipoPersonas");
            DropTable("dbo.TipoDocumentoes");
            DropTable("dbo.Personas");
            DropTable("dbo.Informantes");
            DropTable("dbo.Expedientes");
            DropTable("dbo.Aprendizs");
        }
    }
}
