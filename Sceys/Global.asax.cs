﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Sceys.Migrations;
using Sceys.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Sceys
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Models.SceysContext, Configuration>());
            this.SuperAdmin();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void SuperAdmin()
        {
            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var db = new SceysContext();
            var Username = "fabrica@misena.edu.co";

            this.Role("SuperAdmin", userContext);

            var userASP = userManager.FindByName(Username);

            if (userASP == null)
            {
                userASP = new ApplicationUser
                {
                    UserName = Username,
                    Email = "fabrica@misena.edu.co",
                };
                userManager.Create(userASP, "Fabrica*Tools16");
            }

            userManager.AddToRole(userASP.Id, "SuperAdmin");

        }

        private void Role(string RoleName, ApplicationDbContext userContext)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            if (!roleManager.RoleExists(RoleName))
            {
                roleManager.Create(new IdentityRole(RoleName));
            }
        }
    }
}
