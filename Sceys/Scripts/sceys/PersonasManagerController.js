﻿$(document).ready(function () {//cuando la página este cargada. 

    //Funcionalidad para consultar todos los centros de una regional 
    $("#RegionalId").change(function () {
        var RegionalId = $("#RegionalId option:selected").val();//de la vista tomamos el RegionalId del select. 

        $.ajax({
            type: "POST",
            url: "../PersonasManager/ListarCentros",
            contentType: "application/json; charset=utf-8",
            data: '{RegionalId:' + RegionalId + '}',
            dataType: "json",
            success: function (datos) {
                $("#CentroId").empty();
                var options = "<option value = '0'> Seleccione Uno...</option>";
                for (var i = 0; i < datos.length; i++) {
                    options += "<option value = " + datos[i].centroId + ">" + datos[i].nombreCentro + "</option>";
                }
                $("#CentroId").append(options);


            },
            error: function () {
                alert("Error: Not Exists person!!! Sweet Alert in progress");
            }
        });
    });


    //Listar programas de formación de cada centro.
    $("#CentroId").change(function () {
        var centroId = $("#CentroId option:selected").val();

        $.ajax({
            type: "POST",
            url: "../PersonasManager/consultarPrograma",
            contentType: "application/json; charset=utf-8",
            data: '{capitulo:' + centroId + '}',
            dataType: "json",

            success: function (datos) {
                $("#CentroId").empty();
                var options = "<option value = '0'> Seleccione Uno...</option>";
                for (var i = 0; i < datos.length; i++) {
                    options += "<option value = " + datos[i].CentroId + ">" + datos[i].Mombre + "</option>";
                }
                $("#CentroId").append(options);
            },
            error: function () {
                alert("Error: Not Exists person!!! Sweet Alert in progress");
            }

        });
    })


    //Registrar Aprendiz x Instructor
    $("#registrarAprendiz").click(function () {
        //Recolección de información
        var aprendiz = {};
        aprendiz.TipoDocumentoId = $("#TipoDocumentoId option:selected").val();
        aprendiz.Documento = $("#Documento").val();
        aprendiz.PrimerNombre = $("#PrimerNombre").val();
        aprendiz.SegundoNombre = $("#SegundoNombre").val();
        aprendiz.PrimerApellido = $("#PrimerApellido").val();
        aprendiz.SegundoApellido = $("#SegundoApellido").val();
        aprendiz.Correo = $("#Correo").val();
        aprendiz.Direccion = $("#Direccion").val();
        aprendiz.Celular = $("#Celular").val();
        aprendiz.Telefono = $("#Telefono").val();
        aprendiz.Ficha = $("#Ficha").val();
        aprendiz.RegionalId = $("#RegionalId option:selected").val();
        aprendiz.CentroId = $("#CentroId option:selected").val();

        $.ajax({
            type: "POST",
            url: "/Personas/registrarAprendizXInstructor",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(aprendiz), //formato para enviar un json, se crea un model en el controlador
            dataType: "json",
            success: function (response) {
                if (response > 0) {
                    swal({
                        title: "Registro Exitoso!!",
                        text: "El Aprendiz se registró correctamente!!",
                        type: "success",
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        setTimeout(function () {
                            window.location.href = '/Personas/registrarAprendiz';
                        }, 1000);
                    });
                }
            },
            error: function () {
                sweetAlert("Oops...", "Error al intentar realizar el registro!!!", "error");
            }
        });
    });
});
