﻿/*
    *-------------------------------------------------------------------------
    * Controlador javascript con jquery, ques es transversal a las distintas
    * funcionalidades de Expedientes, en las distintas vistas implicadas como:
    * registrar, editar y otras.
    *-------------------------------------------------------------------------
*/

$(document).ready(function () {
    /*
        *-------------------------------------------------------------------------
        * Funcionalidad para autocompletar por documento del aprendiz en el
        * formulario de registrarQeuja, que hace unicamente el instructor.
        *-------------------------------------------------------------------------
    */
    $("#documentoAprendiz").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../ExpedientesManager/consultaAprendiz",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.documento + " - " + item.nombre,
                            value: item.documento
                        };
                    }))
                }
            })
        }
    });
    /*
        *-------------------------------------------------------------------------
        * funcionalidad para llenar campos cuando se presione click sobre el
        * icono buscar del formulario registrar queja que unicamente realiza
        * el instructor.
        *-------------------------------------------------------------------------
    */
    $('.agregarAprendiz').click(function () {
        var documentoAprendiz = $("#documentoAprendiz").val();
        $.ajax({
            type: "POST",
            url: "../ExpedientesManager/consultaAprendiz",
            contentType: "application/json; charset=utf-8",
            data: '{term:' + documentoAprendiz + '}',
            dataType: "json",
            success: function (data) {
                $("#nombreAprendiz").val(data[0].nombre);
                $("#correoAprendiz").val(data[0].correo);
                $("#fichaAprendiz").val(data[0].ficha);
                $("#programaAprendiz").val(data[0].programa);
            },
            error: function () {
                alert("Error: Not Exists person!!! Sweet Alert in progress");
            }
        });
    });

    //Autocompletar receptor
    $("#documentoReceptor").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../ExpedientesManager/consultaReceptor",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.documento + " - " + item.nombre,
                            value: item.documento
                        };
                    }))
                }
            })
        }
    });

    //add informante
    $('.addInformante').click(function () {
        agregarFila("#docInformante", "addInformante", "Informante");
    });

    // add aprendiz
    $('.addAprendiz').click(function () {
        agregarFila("#aprendiz", "addAprendiz", "Aprendiz");
    });

    //function remov row
    $(document).on('click', '.input-remove-row', function () {
        var tr = $(this).closest('tr');
        tr.fadeOut(200, function () {
            tr.remove();
        });
    });

    //Autocomplete informante
    $("#docInformante").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../ExpedientesManager/consultaPersonas",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.documento + " - " + item.nombre,
                            value: item.documento
                        };
                    }))
                }
            })
        }
    });

    //Autocomplete aprendiz
    $("#aprendiz").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../ExpedientesManager/consultaAprendiz",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.documento + " - " + item.nombre,
                            value: item.documento
                        };
                    }))
                }
            })
        }
    });

    //Consultar Documentacion
    $("#suministra").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "../ExpedientesManager/consultaPersonas",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.documento + " - " + item.nombre,
                            value: item.documento
                        };
                    }))
                }
            })
        }
    });

    //Consultar articulos por capitulo
    $("#Capitulo").change(function () {
        var capituloId = $("#Capitulo option:selected").val();
        $.ajax({
            type: "POST",
            url: "../ExpedientesManager/consultaArticulo",
            contentType: "application/json; charset=utf-8",
            data: '{capitulo:' + capituloId + '}',
            dataType: "json",
            success: function (data) {
                $("#articulos").empty();
                var options = "<option value = '0'> Seleccione Uno...</option>";
                for (var i = 0; i < data.length; i++) {
                    options += "<option value = '" + data[i]["articuloId"] + "'> Artículo " + data[i]["articuloId"] + "</option>";
                }
                $("#articulos").append(options);
                //Se agregar el capítulo al textArea norma
                $("#normas").append($("#Capitulo option:selected").text() + "; ");
            },
            error: function () {
                alert("Sweet Alert in progress");
            }
        });
    });

    //Consultar numerales por artículos
    $("#articulos").change(function () {
        var articuloId = $("#articulos option:selected").val();
        $.ajax({
            type: "POST",
            url: "../ExpedientesManager/consultarNumeral",
            contentType: "application/json; charset=utf-8",
            data: "{articulos: " + articuloId + "}",
            dataType: "json",
            success: function (data) {
                $("#numeral").empty();
                var options = "<option value = '0'> Seleccione Uno...</option>";
                for (var i = 0; i < data.length; i++) {
                    options += "<option value = '" + data[i]["numeralId"] + "'> Numeral " + data[i]["numeralId"] + "</option>";
                }
                $("#numeral").append(options);
                //agregar artículos al textArea norma
                $("#normas").append($("#articulos option:selected").text() + "\n");
            },
            error: function () {
                alert("Error Sweet Alert in progress");
            }
        });
    });

    //Agregar descripción del numeral al textarea normas
    $("#numeral").change(function () {
        var numeralId = $("#numeral option:selected").val();
        alert(numeralId);

    });
});

//function add row
function agregarFila(inputDocumento, button, tipo) {
    var form_data = {};
    var doc = $(inputDocumento).val();
    $.ajax({
        type: "POST",
        url: "../ExpedientesManager/consultaPersonas",
        contentType: "application/json; charset=utf-8",
        data: '{term:' + doc + '}',
        dataType: "json",
        success: function (data) {
            form_data["status"] = tipo;
            form_data["documento"] = doc;
            form_data["nombre"] = data[0].nombre;
            form_data["correo"] = data[0].correo;
            form_data["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';
            if ($(inputDocumento).val().length > 0) {
                var row = $('<tr></tr>');
                $.each(form_data, function (type, value) {
                    $('<td class="input-' + type + '"></td>').html(value).appendTo(row);
                });
                $('.preview-table > tbody:last').append(row);
                //empty input
                $(inputDocumento).val("");
            }
        },
        error: function () {
            alert("Error: Not Exists person!!! Sweet Alert in progress");
        }
    });
}
