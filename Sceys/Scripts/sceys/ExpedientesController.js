﻿/*
    *-------------------------------------------------------------------------
    * El código esta relacionado con el Controlador correspondiente.
    *-------------------------------------------------------------------------
*/

$(document).ready(function () {
  /*
      *-------------------------------------------------------------------------
      * En caso de adjuntar archivos a una queja, en primer lugar se deben cargar
      * los archivos que pudieron adjuntar otros instructores o el mismo.
      *
      * NOTA:
      * Verificar si se listan también los archivos por parte del aprendiz en defensa
      * de la queja a la cual esta asociado. En caso de que quiera hacer la legitima
      * defensa
      *-------------------------------------------------------------------------
  */

    @if(Model.ExpedienteId>0){
      <text>
        actualizarAdjuntos();
      </text>
    }

    /*
        *-------------------------------------------------------------------------
        * REGISTRAR QUEJA:
        * Se usa el método Create del controlador ExpedienteController.cs
        * en una petición ajax.
        * Los datos del formulario de la vista viewQuejaInstructor.cshtml, se reciben en un objeto
        * que se pasa al método Create para ser iterado.
        *-------------------------------------------------------------------------
    */
    $("#registrarQueja").click(function () {

        //Recolección de información
        var expedienteAprendiz = {};
        expedienteAprendiz.documentoAprendiz = $("#documentoAprendiz").val();
        expedienteAprendiz.tipoFaltaId = $("#Nombre option:selected").val();
        expedienteAprendiz.calificacionId = $("#Descripcion option:selected").val();
        expedienteAprendiz.descripcionQueja = $("#descripcionQueja").val();

        $.ajax({
            type: "POST",
            url: "/Expedientes/registrarQuejaXInstructor",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(expedienteAprendiz), //formato para enviar un json, se crea un model en el controlador
            dataType: "json",
            success: function (ExpedienteId) {
                if (ExpedienteId > 0) {
                    swal({
                        title: "Registro Exitoso!!",
                        text: "La queja se creó correctamente!!",
                        type: "success",
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        setTimeout(function () {
                            window.location.href = '/Expedientes/adjuntarDocumentacion?ExpedienteId=' + ExpedienteId;
                        }, 1000);
                    });
                }
            },
            error: function () {
                sweetAlert("Oops...", "Error al intentar realizar el registro!!!", "error");
            }
        });
    });

    /*
        *-------------------------------------------------------------------------
        * Funcionalidad de agregar documentación para cada expediente
        * la documentación la hace cada instructor, y es responsable por los
        * archivos adjuntos, de ser necesarios ser evaluados en una sesión del comité.
        *-------------------------------------------------------------------------
    */
    $("#frm-adjuntarDocumentacion").submit(function () {
        var form = $(this);
        form.ajaxSubmit({
            dataType: 'JSON',
            type: 'POST',
            url: form.attr('action'),
            success: function (response) {
                if(response ==1){
		  swal({
                        title: "Se adjuntó el archivo!!",
                        text: "La prueba se adjuntó correctamente al expediente del aprendiz!!",
                        type: "success",
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        setTimeout(function () {
                            actualizarAdjuntos();
                        }, 1000);
                    });
		}
            },
            error: function () {
                alert("Error, Sweet alert is not coming yet");
            }
        });
    });
});

function actualizarAdjuntos(){
  $("#tblIndex").empty();
  $("#tblIndex").load("@Url.Content(~/Expedientes/adjuntos?ExpedienteId="+Model.ExpedienteId")");
}
