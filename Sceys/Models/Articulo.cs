﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Articulo
    {     

        public int ArticuloId { get; set; }

        [Display(Name = "Capitulo")]
        public int CapituloId { get; set; }
        
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Articulo")]
        public string Descripcion { get; set; }

        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Articulo")]
        public string Descripcion2 { get; set; }
        
        //virtual
        public virtual Capitulo Capitulo { get; set; }      

        //relations
        public virtual ICollection<ArticuloFalta> ArticuloFalta { get; set; }
        public virtual ICollection<Cita> Cita { get; set; }
        public virtual ICollection<Paragrafo> Paragrafos { get; set; }
        public virtual ICollection<Numeral> Numerals { get; set; }
    }
}