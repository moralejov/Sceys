﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Persona
    {
        [Key]
        public int PersonaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Tipo de documento")]
        public int TipoDocumentoId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Numero de documento")]
        public string Documento { get; set; }

        [Display(Name = "Documento")]
        public string DocumentoIdentidad { get { return string.Format("{0}. {1}", TipoDocumento.Abreviacion, this.Documento); } }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Primer Nombre")]
        public string PrimerNombre { get; set; }

        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Primer Apellido")]
        public string PrimerApellido { get; set; }

        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Segundo Apellido")]
        public string SegundoApellido { get; set; }


        [Display(Name = "Nombre Completo")]
        public string NombreCompleto { get { return string.Format("{0} {1} {2} {3}"  , this.PrimerNombre, this.SegundoNombre, this.PrimerApellido, this.SegundoApellido) ; }  }

        [Display(Name = "Nombre y Apellido")]
        public string NombreCorto { get { return string.Format("{0} {1}", this.PrimerNombre, this.PrimerApellido); } }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 5)]
        [EmailAddress(ErrorMessage = "{0} debe tener un formato Email")]
        [Display(Name = "Correo Electrónico")]
        public string Correo { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [StringLength(15, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 10)]
        public string Celular { get; set; }

        [StringLength(15, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 7)]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [Display(Name = "Número de contacto")]
        public string NumeroContacto { get { return string.Format("{0} - {1}", this.Telefono, this.Celular); } }

        public bool Estado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Tipo de Persona")]
        public int TipoPersonaId { get; set; }

        //virtuals models
        public virtual TipoPersona TipoPersona { get; set; }

        public virtual TipoDocumento TipoDocumento { get; set; }

        //ICollections
        public virtual ICollection<RecepcionDocumento> RecepcionDocumentos { get; set; }

        public virtual ICollection<Informante> Informantes { get; set; }

        public virtual ICollection<Aprendiz> Aprendizs { get; set; }

        public virtual ICollection<Documentacion> Documentacions { get; set; }


    }
}