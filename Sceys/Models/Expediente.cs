﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Expediente
    {
        [Key]
        public int ExpedienteId { get; set; }             

        public int AprendizId { get; set; }        

        public DateTime Fecha { get; set; }

        public int Estado { get; set; }

        public string descripcionQueja { get; set;}

        //Se relaciona con     
        public virtual Aprendiz Aprendiz { get; set; }              

        //Se relaciona en:
        public virtual ICollection<Falta> Faltas { get; set; }

        public virtual ICollection<RecepcionDocumento> RecepcionDocumentos { get; set; }

        public virtual ICollection<Documentacion> Documentacions { get; set; }
    }
}