﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class SubNumeral
    {
        [Key]
        public int SubNumeralId { get; set; }

        [Display(Name = "Numeral")]
        public int NumeralId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Sub Numeral")]
        public string Descripcion { get; set; }

        //virtual
        public virtual Numeral Numeral { get; set; }
    }
}