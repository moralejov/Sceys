﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class NumeralParagrafo
    {
        [Key]
        public int NumeralParagrafoId { get; set; }

        [Display(Name = "Paragrafo")]
        public int ParagrafoId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8000, ErrorMessage = "{0} debe tener máximo {1} caracteres", MinimumLength = 1)]
        [Display(Name = "Numeral Del Paragrafo")]
        public string Descripcion { get; set; }

        //virtual
        public virtual Paragrafo Paragrafo { get; set; }
    }
}