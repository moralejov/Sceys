﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class ProgramaCentro
    {
        [Key]
        public int ProgramaCentroId { get; set; }

        [Display(Name = "programa")]
        public int ProgramaId { get; set; }

        [Display(Name = "Centro")]
        public int CentroId { get; set; }

        public virtual Programa Programa { get; set; }

        public virtual Centro Centro { get; set; }

        public virtual ICollection<Aprendiz> Aprendizs { get; set; }
    }
}