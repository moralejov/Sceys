﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class TipoFalta
    {
        [Key]
        public int TipoFaltaId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set;}

        //relations
        public ICollection<Falta> Falta { get; set; }
    }
}