﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class SceysContext:DbContext
    {
        public SceysContext() : base("DefaultConnection")
        {
            base.Configuration.ProxyCreationEnabled = false;
            base.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<RecepcionDocumento> RecepcionDocumentoes { get; set; }

        public System.Data.Entity.DbSet<Informante> Informantes { get; set; }

        public System.Data.Entity.DbSet<Persona> Personas { get; set; }

        public System.Data.Entity.DbSet<TipoDocumentacion> TipoDocumentacions { get; set; }

        public System.Data.Entity.DbSet<Documentacion> Documentacions { get; set; }

        public System.Data.Entity.DbSet<Expediente> Expedientes { get; set; }

        public System.Data.Entity.DbSet<Aprendiz> Aprendizs { get; set; }

        public System.Data.Entity.DbSet<TipoPersona> TipoPersonas { get; set; }

        public System.Data.Entity.DbSet<TipoDocumento> TipoDocumentos { get; set; }

        public System.Data.Entity.DbSet<Regional> Regionals { get; set; }

        public System.Data.Entity.DbSet<Centro> Centros { get; set; }

        public System.Data.Entity.DbSet<TipoPrograma> TipoProgramas { get; set; }

        public System.Data.Entity.DbSet<Programa> Programas { get; set; }

        public System.Data.Entity.DbSet<ProgramaCentro> ProgramaCentros { get; set; }

        //Expediente
        public System.Data.Entity.DbSet<Falta> Faltas { get; set; }
        public System.Data.Entity.DbSet<TipoFalta> TipoFaltas { get; set; }
        public System.Data.Entity.DbSet<Calificacion> Calificacions { get; set; }

        //Reglamento del aprendiz
        public System.Data.Entity.DbSet<Articulo> Articulos { get; set; }
        public System.Data.Entity.DbSet<ArticuloFalta> ArticuloFaltas { get; set; }
        public System.Data.Entity.DbSet<Capitulo> Capitulos { get; set; }
        public System.Data.Entity.DbSet<Paragrafo> Paragrafos { get; set; }
        public System.Data.Entity.DbSet<NumeralParagrafo> NumeralParagrafos { get; set; }
        public System.Data.Entity.DbSet<Cita> Citas { get; set; }
        public System.Data.Entity.DbSet<Numeral> Numerals { get; set; }
        public System.Data.Entity.DbSet<SubNumeral> SubNumeral { get; set; }


      	//Método que se encuentra en el modelo Documentación, para usarse en la vista tipo PartialViewResult.
      	//Usado en el método adjuntos del controlador ExpedientesController.cs
       // public virtual DbSet<Documentacion> Adjuntos { get; set; }

    }
}
