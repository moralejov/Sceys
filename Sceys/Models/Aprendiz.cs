﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Aprendiz
    {
        [Key]
        public int AprendizId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Persona")]
        public int PersonaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Programa")]
        public int ProgramaCentroId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 4)]
        public string Ficha { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Tipo de programa")]
        public int TipoProgramaId { get; set; }


        public virtual Persona Persona { get; set; }

        public virtual TipoPrograma TipoPrograma { get; set; }

        public virtual ProgramaCentro ProgramaCentro { get; set; }

        //ICollection
        public ICollection<Expediente> Expediente { get; set; }
    }
}