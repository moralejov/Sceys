﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class TipoPrograma
    {
        [Key]
        public int TipoProgramaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(100, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Descripción")]
        public string  Descripcion { get; set; }

        public virtual ICollection<Aprendiz> Aprendiz { get; set; }
    }
}