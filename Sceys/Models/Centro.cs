﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Centro
    {
        [Key]
        public int CentroId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(250, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Nombre del centro")]
        public string Nombre { get; set; }

        [Display(Name = "Regional")]
        public int RegionalId { get; set; }

        public virtual Regional Regional { get; set; }

        public virtual ICollection<ProgramaCentro> ProgramaCentros { get; set; }
    }
}