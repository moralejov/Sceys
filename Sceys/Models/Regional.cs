﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Regional
    {
        [Key]
        public int RegionalId { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Nombre de la Regional")]
        public string Nombre { get; set; }

        public virtual ICollection<Centro> Centros { get; set; }
    }
}