﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class PersonaViewEdit
    {
        public int PersonaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Tipo de documento")]
        public int TipoDocumentoId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Numero de documento")]
        public string Documento { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Primer Nombre")]
        public string PrimerNombre { get; set; }

        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Primer Apellido")]
        public string PrimerApellido { get; set; }

        [StringLength(20, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 2)]
        [Display(Name = "Segundo Apellido")]
        public string SegundoApellido { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 5)]
        [EmailAddress(ErrorMessage = "{0} debe tener un formato Email")]
        [Display(Name = "Correo Electrónico")]
        public string Correo { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [StringLength(15, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 10)]
        public string Celular { get; set; }

        [StringLength(15, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 7)]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Tipo de Persona")]
        public int TipoPersonaId { get; set; }

        [Display(Name = "Aprendiz")]
        public int AprendizId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Programa")]
        public int ProgramaId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 4)]
        public string Ficha { get; set; }

        [Display(Name = "Tipo de Programa")]
        public int TipoProgramaId { get; set; }

        [Display(Name = "Tipo de Persona")]
        public string TipoPersonaDescripcion { get; set; }

        [Display(Name = "Tipo de Documento")]
        public string TipoDocumentoDescripcion { get; set; }

        [Display(Name = "Centro")]
        public int CentroId { get; set; }


        [Display(Name = "Regional")]
        public int RegionalId { get; set; }
    }
}