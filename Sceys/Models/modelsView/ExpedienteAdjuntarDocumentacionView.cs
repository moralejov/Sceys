﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sceys.Models.modelsView
{
    public class ExpedienteAdjuntarDocumentacionView
    {
        //Información del aprendiz y el expediente al cual se le van a adjuntar los documentos de la queja
        public int ExpedienteId {get;set;}

        public string fecha { get; set; }   

        public int estado { get; set; }    

        public string nombreAprendiz { get; set; }

        public string documentoAprendiz { get; set; }   

        public string correoAprendiz { get; set; }
    }
}