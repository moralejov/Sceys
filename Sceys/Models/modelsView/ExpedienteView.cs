﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class ExpedienteView
    {
        public int ExpedienteId { get; set; }

        public string documento { get; set; }

        public string aprendiz { get; set; }

        public string documentoAprendiz { get; set;}

        public string documentoReceptor { get; set; }

        public string contacto { get; set; }

        public DateTime fecha { get; set; }

        public int estado { get; set; }

        public string descripcionGeneral { get; set; }

        public int tipoFaltaId { get; set; }

        public int calificacionId { get; set; }

        public string descripcionFalta { get; set; }
        
        public string descripcionQueja { get; set; }
    }
}