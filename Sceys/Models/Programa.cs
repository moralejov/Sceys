﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sceys.Models
{
    public class Programa
    {
        [Key]
        public int ProgramaId { get; set; }
                
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(250, ErrorMessage = "{0} debe tener máximo {1} caracteres y mínimo {2} caracteres", MinimumLength = 4)]
        [Display(Name = "Nombre del Programa")]
        public string Nombre { get; set; }

        

        public virtual ICollection<ProgramaCentro> ProgramaCentros { get; set; }
    }
}