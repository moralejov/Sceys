﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Sceys.Models;
using Sceys.Models.modelsView;
using System.Web;
using System.IO;

namespace Sceys.Controllers
{
    public class ExpedientesController : Controller
    {
        private SceysContext db = new SceysContext();

        // GET: Expedientes
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {

                //tipos de falta por defecto
                this.LlenarTipoFalta("Académica", "");
                this.LlenarTipoFalta("Disciplinaria", "");
                this.LlenarTipoFalta("Académica y Disciplinaria", "");

                //calificación por defecto para cada tipo de falta
                this.LlenarCalificacion("Leve");
                this.LlenarCalificacion("Grave");
                this.LlenarCalificacion("Gravisima");

                //guardar cambios en la base de datos
                try
                {
                    db.SaveChanges();
                }
                catch (Exception msg)
                {
                    TempData["Errors"] = msg.Message;
                    return RedirectToAction("Index", "Home");
                }

                var expedientes = (from Aprendiz in db.Aprendizs
                                   join Persona in db.Personas on Aprendiz.PersonaId equals Persona.PersonaId
                                   join Expediente in db.Expedientes on Aprendiz.AprendizId equals Expediente.AprendizId
                                   orderby Expediente.Estado ascending

                                   select new ExpedienteView
                                   {
                                       ExpedienteId = Expediente.ExpedienteId,
                                       documento = Persona.Documento,
                                       aprendiz = Persona.PrimerNombre + " " + Persona.SegundoNombre + " " + Persona.PrimerApellido + " " + Persona.SegundoNombre,
                                       contacto = Persona.Correo,
                                       fecha = Expediente.Fecha,
                                       estado = Expediente.Estado,
                                       descripcionQueja = Expediente.descripcionQueja
                                   }).OrderBy(p => p.estado).ThenBy(p => p.fecha);

                return View(expedientes);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Expedientes/Details/5
        public ActionResult Details(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                /*
                    * La primera parte para ver los detalles del expediente de un aprendiz
                    * consiste en realizar la consulta de la información que se encuentra
                    * en las distintas tablas que se relacionan con la tabla Expedientes
                */

                // Se consulta el Expediente del aprendiz
                var expediente = db.Expedientes.Find(id);
                if (expediente == null)
                {
                    return HttpNotFound();
                }

                //Se consulta la información del aprendiz. En este caso, el nombre del aprendiz.
                var aprendiz = (
                    from Aprendiz in db.Aprendizs
                    join Expediente in db.Expedientes on Aprendiz.AprendizId equals Expediente.AprendizId
                    join Persona in db.Personas on Aprendiz.PersonaId equals Persona.PersonaId
                    where Expediente.ExpedienteId == id
                    select new
                    {
                        documento = Persona.Documento,
                        nombre = Persona.PrimerNombre + " " + Persona.SegundoNombre + " " + Persona.PrimerApellido + " " + Persona.SegundoApellido,
                        correo = Persona.Correo,
                        ficha = Aprendiz.Ficha,
                        programa = (from programaxcentro in db.ProgramaCentros
                                    join Aprendix in db.Aprendizs on programaxcentro.ProgramaCentroId equals Aprendix.ProgramaCentroId
                                    join programa in db.Programas on programaxcentro.ProgramaId equals programa.ProgramaId
                                    where Aprendix.PersonaId == Persona.PersonaId
                                    select programa.Nombre
                                          ).FirstOrDefault()
                    }
                    ).FirstOrDefault();

                //Se consulta el tipo de falta que tiene el aprendiz, desde la tabla TipoFalta
                var tipoFalta = (
                            from Falta in db.Faltas
                            join TipoFalta in db.TipoFaltas on Falta.TipoFaltaId equals TipoFalta.TipoFaltaId
                            join Expediente in db.Expedientes on Falta.ExpedienteId equals Expediente.ExpedienteId
                            where Expediente.ExpedienteId == id
                            select TipoFalta.Nombre
                           ).FirstOrDefault();

                //Se consulta calificación de la falta que tiene el aprendiz, desde la tabla CalificacionFalta
                var calificacionFalta = (
                    from Falta in db.Faltas
                    join Calificacion in db.Calificacions on Falta.CalificacionId equals Calificacion.CalificacionId
                    join Expediente in db.Expedientes on Falta.ExpedienteId equals Expediente.ExpedienteId
                    where Expediente.ExpedienteId == id
                    select Calificacion.Descripcion
                    ).FirstOrDefault();

                //Se consulta de la tabla Faltas, la descripción de la normatividad
                var DescripcionFaltas = (
                from Falta in db.Faltas
                join Expediente in db.Expedientes on Falta.ExpedienteId equals Expediente.ExpedienteId
                where Expediente.ExpedienteId == id
                select Falta.DescripcionFaltas
                           ).FirstOrDefault();

                //Se llenan los campos que serán mostrados en la vista con las consultas previas que se hicieron.
                var fecha = Convert.ToString(expediente.Fecha);
                var expedientes = new ExpedienteDetailsView
                {
                    ExpedienteId = expediente.ExpedienteId,
                    documentoAprendiz = aprendiz.documento,
                    nombreAprendiz = aprendiz.nombre,
                    correoAprendiz = aprendiz.correo,
                    fichaAprendiz = aprendiz.ficha,
                    programaAprendiz = aprendiz.programa,
                    fecha = fecha,
                    Estado = expediente.Estado,
                    descripcionQueja = expediente.descripcionQueja,
                    tipoFalta = tipoFalta,
                    calificacionFalta = calificacionFalta,
                    DescripcionFaltas = DescripcionFaltas
                };

                return View(expedientes);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Expedientes/Create
        public ActionResult Create()
        {

            //Valores para los DropDownList
            ViewBag.Nombre = new SelectList(db.TipoFaltas, "TipoFaltaId", "Nombre");
            ViewBag.Descripcion = new SelectList(db.Calificacions, "CalificacionId", "Descripcion");

            //Información de los capítulos del reglamento del aprendiz
            ViewBag.Capitulo = new SelectList(db.Capitulos, "CapituloId", "Descripcion");

            return View();

        }

        // POST: Expedientes/Create
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(ExpedienteView expedienteView, List<RecepcionDocumento> datos)
        {
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        // GET: Expedientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Expediente expediente = db.Expedientes.Find(id);
                if (expediente == null)
                {
                    return HttpNotFound();
                }
                ViewBag.AprendizId = new SelectList(db.Aprendizs, "AprendizId", "Ficha", expediente.AprendizId);
                //ViewBag.InformanteId = new SelectList(db.Informantes, "InformanteId", "Descripcion", expediente.InformanteId);
                //ViewBag.RecepcionDocumentoId = new SelectList(db.RecepcionDocumentoes, "RecepcionDocumentoId", "RecepcionDocumentoId", expediente.RecepcionDocumentoId);
                return View(expediente);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Expedientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Estado")] Expediente expediente)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(expediente).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.AprendizId = new SelectList(db.Aprendizs, "AprendizId", "Ficha", expediente.AprendizId);
                //ViewBag.InformanteId = new SelectList(db.Informantes, "InformanteId", "Descripcion", expediente.InformanteId);
                //ViewBag.RecepcionDocumentoId = new SelectList(db.RecepcionDocumentoes, "RecepcionDocumentoId", "RecepcionDocumentoId", expediente.RecepcionDocumentoId);
                return View(expediente);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Expedientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Expediente expediente = db.Expedientes.Find(id);
                if (expediente == null)
                {
                    return HttpNotFound();
                }
                return View(expediente);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: Expedientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Expediente expediente = db.Expedientes.Find(id);
                db.Expedientes.Remove(expediente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //agregar TipoFalta por defecto
        private void LlenarTipoFalta(string nombre, string descripcion)
        {
            var existe = db.TipoFaltas.Where(des => des.Nombre.Equals(nombre));
            if (existe.Count() == 0)
            {
                var tipoFalta = new TipoFalta
                {
                    Nombre = nombre,
                    Descripcion = descripcion

                };
                db.TipoFaltas.Add(tipoFalta);
            }
        }

        //agregar Calificacion por defecto
        private void LlenarCalificacion(string descripcion)
        {
            var existe = db.Calificacions.Where(des => des.Descripcion.Equals(descripcion));
            if (existe.Count() == 0)
            {
                var calificacion = new Calificacion
                {
                    Descripcion = descripcion
                };
                db.Calificacions.Add(calificacion);
            }
        }


        //Metodo que genera la vista para registrar queja por el instructor.
        public ActionResult registrarQueja()
        {
            if (User.Identity.IsAuthenticated)
            {
                //Valores para los DropDownList
                ViewBag.Nombre = new SelectList(db.TipoFaltas, "TipoFaltaId", "Nombre");
                ViewBag.Descripcion = new SelectList(db.Calificacions, "CalificacionId", "Descripcion");

                //Información de los capítulos del reglamento del aprendiz
                ViewBag.Capitulo = new SelectList(db.Capitulos, "CapituloId", "Descripcion");

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public JsonResult registrarQuejaXInstructor(ExpedienteRegistraQuejaXInstructorView registrarQueja)
        {
            //Información adicional
            var fecha = DateTime.Now;

            if (registrarQueja != null)
            {
                var aprendizId = (from Personas in db.Personas
                                  join Aprendices in db.Aprendizs on Personas.PersonaId equals Aprendices.PersonaId
                                  where Personas.Documento == registrarQueja.documentoAprendiz
                                  select Aprendices.AprendizId
                       ).FirstOrDefault();
                var Expedientes = new Expediente
                {
                    AprendizId = aprendizId,
                    Fecha = fecha,
                    Estado = 0,
                    descripcionQueja = registrarQueja.descripcionQueja
                };
                db.Expedientes.Add(Expedientes);
                db.SaveChanges();

                var Faltas = new Falta
                {
                    ExpedienteId = Expedientes.ExpedienteId,
                    TipoFaltaId = registrarQueja.tipoFaltaId,
                    CalificacionId = registrarQueja.calificacionId,
                    DescripcionFaltas = "Las normas del reglamento del aprendiz estan en progreso"
                };

                db.Faltas.Add(Faltas);
                db.SaveChanges();
                return Json(Expedientes.ExpedienteId, JsonRequestBehavior.AllowGet);
            }
            return Json(3, JsonRequestBehavior.AllowGet);
        }

        public ActionResult adjuntarDocumentacion(int? ExpedienteId)
        {
            //información del expediente
            var expediente = db.Expedientes.Find(ExpedienteId);
            if (expediente == null)
            {
                return HttpNotFound();
            }


            //información del aprendiz
          var aprendiz = (
          from Aprendiz in db.Aprendizs
          join Expediente in db.Expedientes on Aprendiz.AprendizId equals Expediente.AprendizId
          join Persona in db.Personas on Aprendiz.PersonaId equals Persona.PersonaId
          where Expediente.ExpedienteId == ExpedienteId
          select new
          {
              documento = Persona.Documento,
              nombre = Persona.PrimerNombre + " " + Persona.SegundoNombre + " " + Persona.PrimerApellido + " " + Persona.SegundoApellido,
              correo = Persona.Correo,
              ficha = Aprendiz.Ficha,
              programa = (from programaxcentro in db.ProgramaCentros
                          join Aprendix in db.Aprendizs on programaxcentro.ProgramaCentroId equals Aprendix.ProgramaCentroId
                          join programa in db.Programas on programaxcentro.ProgramaId equals programa.ProgramaId
                          where Aprendix.PersonaId == Persona.PersonaId
                          select programa.Nombre
                                ).FirstOrDefault()
          }
          ).FirstOrDefault();
            var fecha = Convert.ToString(expediente.Fecha);
            var expedienteInfo = new ExpedienteAdjuntarDocumentacionView
            {
                ExpedienteId = expediente.ExpedienteId,
                fecha = fecha,
                estado = expediente.Estado,
                documentoAprendiz = aprendiz.documento,
                nombreAprendiz = aprendiz.nombre,
                correoAprendiz = aprendiz.correo
            };

            return View(expedienteInfo);
        }

        public JsonResult adjuntarDocumentacionXInstructor(int? ExpedienteId, string comentarioDocumentoAdjunto, HttpPostedFileBase archivo)
        {
            //Información general
            var fecha = DateTime.Now;
	    var expedienteId = Convert.ToInt32(ExpedienteId);
	    var correo = User.Identity.Name;
    	    var personaId = (from Personas in db.Personas
			    where Personas.Correo == correo
			    select Personas.PersonaId).FirstOrDefault();
	    var descripcion = Convert.ToString(comentarioDocumentoAdjunto);


            if (archivo != null)
            {
                string adjunto = DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(archivo.FileName);
                archivo.SaveAs(Server.MapPath("~/Uploads/" + adjunto));

                var documentacion = new Documentacion
                {
                    ExpedienteId = expedienteId,
                    TipoDocumentacionId = 1,
                    PersonaId = personaId,
                    nombreArchivo = adjunto,
                    comentarioArchivo = descripcion,
                    fecha = fecha
                };

                db.Documentacions.Add(documentacion);
                db.SaveChanges();

                //if was correct
                return Json(1, JsonRequestBehavior.AllowGet);
            }

            //if was error
            return Json(0, JsonRequestBehavior.AllowGet);
        }

      	public PartialViewResult Adjuntos(int ExpedienteId){

            //return PartialView(Documentacion.Adjuntos(ExpedienteId));
            return null;
      	}

    }
}
